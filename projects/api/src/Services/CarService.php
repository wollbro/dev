<?php

declare(strict_types=1);

namespace CarBuddy\Services;

use CarBuddy\Models\Car;
use Doctrine\ORM\EntityManager;

class CarService {
  private $entityManager;

  public function __construct(EntityManager $entityManager) {
    $this->entityManager = $entityManager;
  }

  public function getAllCars(): array {
    $repository = $this->entityManager->getRepository(Car::class);
    return $repository->findAll();
  }
}
