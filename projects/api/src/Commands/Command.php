<?php

declare(strict_types=1);

namespace CarBuddy\Commands;

interface Command {
  public function execute();
}
