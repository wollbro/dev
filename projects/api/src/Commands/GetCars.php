<?php

declare(strict_types=1);

namespace CarBuddy\Commands;

class GetCars implements Command {
  public function execute(?string $carId = null) : array {
    if($carId !== null)
      return [
        'id' => $carId
      ];

    return [1,2];
  }
}
