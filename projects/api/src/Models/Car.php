<?php

namespace CarBuddy\Models;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="cars")
 */
class Car {
  /**
   * @ORM\Id
   * @ORM\Column(type="integer")
   * @ORM\GeneratedValue
   */
  private $id;

  /**
   * @ORM\Column(type="string")
   */
  private string $brand;

  /**
   * @ORM\Column(type="string")
   */
  private string $model;
  
  public function getId() {
    return $this->id;
  }
}
