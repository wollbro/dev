<?php

declare(strict_types=1);

namespace CarBuddy\System;

use Auryn\Injector;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

class Database {
  public static function setup(Injector $injector) {
    $paths = [__DIR__.'/../Models'];
    $isDevMode = true;
    
    // the connection configuration
    $dbParams = array(
      'driver'   => 'pdo_mysql',
      'host'     => 'mysql',
      'user'     => 'carbuddy',
      'password' => 'carbuddy',
      'dbname'   => 'carbuddy',
    );
    
    $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, null, null, false);
    $entityManager = EntityManager::create($dbParams, $config);

    $injector->delegate(EntityManager::class, function()use($entityManager) {
      return $entityManager;
    });
    $injector->share(EntityManager::class);
  }
}
