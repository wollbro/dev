<?php

declare(strict_types=1);

namespace CarBuddy\Network;

use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;
use \Auryn\Injector;

class Router {
  private $injector;

  public function __construct(Injector $injector) {
    $this->injector = $injector;
  }

  public function route(Request $request): Response {
    $parts = explode('/', $request->getPathInfo());
    $parts = array_filter($parts);
    $parts = array_values($parts);
   
    $modelId = $parts[1] ?? null;
    if($modelId !== null)
      $request->attributes->set('id', $modelId);

    $controllerName = $this->normalizeString($parts[0]);
    $controllerPath = "CarBuddy\\Controllers\\{$controllerName}Controller";

    $method = $request->server->get('REQUEST_METHOD');
    $method = $this->normalizeString($method);
    
    $controller = $this->injector->make($controllerPath);
    return $controller->$method($request);
  }

  private function normalizeString(string $string) {
    $string = strtolower($string);
    $string = ucfirst($string);
    return $string;
  }
}
