<?php

declare(strict_types=1);

namespace CarBuddy\Controllers;

use CarBuddy\Commands\GetCars;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CarsController implements Controller {
  private GetCars $getCars;

  public function __construct(GetCars $getCars) {
    $this->getCars = $getCars;
  }

  public function Get(Request $request): Response {
    try {
      $modelId = $request->attributes->get('id');
      $cars = $this->getCars->execute($modelId);

      return new Response(json_encode($cars), 200);
    } catch(\Exception $ex) {
      return new Response(json_encode([
        'msg' => $ex->getMessage(),
        'stacktrach' => $ex->getTraceAsString()
      ]), 500);
    }
  }
}
