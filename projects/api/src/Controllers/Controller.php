<?php

declare(strict_types=1);

namespace CarBuddy\Controllers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface Controller {
  public function Get(Request $request): Response;
}
