#!/bin/bash

helpFunction()
{
   echo ""
   echo "Usage: $0 -f [Path to the test file you want to run]"
   exit 1
}

while getopts "f:" opt
do
   case "$opt" in
      f ) filePath="$OPTARG" ;;
      ? ) helpFunction ;;
   esac
done

# Print helpFunction in case parameters are empty
if [ -z "$filePath" ]
then
   helpFunction
fi

# Removes src from file path, it doesn't exist in the docker container
filePath=${filePath//src\//};

docker-compose exec php sh -c "vendor/bin/phpunit $filePath"
