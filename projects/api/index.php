<?php

declare(strict_types=1);

namespace CarBuddy;

use \Symfony\Component\HttpFoundation\Request;

require_once('bootstrap.php');

$request = Request::createFromGlobals();
$response = $router->route($request);

$response->send();
