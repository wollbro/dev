<?php

class RoboFile extends \Robo\Tasks {
    public function testsUnit() {
        $this->taskExec("vendor/bin/phpunit tests")->run();
    }

    public function testsPsalm() {
        $this->taskExec("vendor/bin/psalm")->run();
    }
}
