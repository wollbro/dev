<?php

declare(strict_types=1);

namespace CarBuddy\Tests\Commands;

use CarBuddy\Commands\GetCars;
use PHPUnit\Framework\TestCase;

class GetCarsTest extends TestCase {
  private $command;

  public function setUp(): void {
    $this->command = new GetCars();
  }

  public function testFirstTest() {
    $cars = $this->command->execute();
    $this->assertCount(2, $cars);
  }
}
