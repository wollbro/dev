<?php

declare(strict_types=1);

namespace CarBuddy\Services;

use CarBuddy\Models\Car;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use PHPUnit\Framework\TestCase;

class CarServiceTest extends TestCase {
  private CarService $service;
  private EntityManager $entityManger;

  public function setUp(): void {
    $this->entityManger = $this->createMock(EntityManager::class);
    $this->service = new CarService($this->entityManger);
  }

  public function testGetAllCars() {
    $repository = $this->createMock(EntityRepository::class);
    $repository
      ->method('findAll')
      ->willReturn([
        new Car(),
        new Car()
      ]);

    $this->entityManger
      ->method('getRepository')
      ->willReturn($repository);

    $cars = $this->service->getAllCars();
    $this->assertCount(2, $cars);
  }
}
