<?php

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;

// replace with file to your own project bootstrap
require_once 'api/bootstrap.php';

// replace with mechanism to retrieve EntityManager in your app
$entityManager = $injector->make(EntityManager::class);

return ConsoleRunner::createHelperSet($entityManager);
