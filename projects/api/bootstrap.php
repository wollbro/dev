<?php

use CarBuddy\System\Database;
use CarBuddy\Network\Router;

require_once('vendor/autoload.php');

$injector = new \Auryn\Injector();
$router = $injector->make(Router::class);

Database::setup($injector);
